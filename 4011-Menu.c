/* \file 4011-Menu.c
 *
 * \brief  Demonstrate a simple menu on the 30F4011
 *
 *
 * Author: jjmcd
 *
 * Created on September 4, 2012, 5:10 PM
 */

/******************************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2012 by John J. McDonough, WB8RCR
 *
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 *
 *****************************************************************************/

#include <xc.h>

#include <stdio.h>
#include <stdlib.h>
#include "../4011-LCDlib-D.X/lcd.h"
#include "../4011-LCDlib-D.X/delay.h"

// Configuration fuses
_FOSC (XT_PLL16 & PRI)                  // 7.3728 rock * 16 = 118MHz
_FWDT (WDT_OFF)                         // Watchdog timer off
_FBORPOR (PWRT_16 & PBOR_OFF & MCLR_EN) // Brownout off, powerup 16ms
_FGS (GWRP_OFF & CODE_PROT_OFF)         // No code protection

#define Delay_500mS_Cnt Delay_1S_Cnt/2
#define Delay_2S_Cnt Delay_1S_Cnt*2

        //	Function Prototypes
int main (void);
void Initialize(void);

//#define MENU

int nMenPos[6] = { 1, 6, 11, 65, 70, 75 };

void makeSelection( int nSel )
{
        LCDposition( nMenPos[nSel] );
        Delay( Delay_500mS_Cnt );
        switch ( nSel )
          {
          case 0:
            _LATD1 = 0;
            break;
          case 1:
            _LATD3 = 0;
            break;
          case 2:
            _LATD2 = 0;
            break;
          case 3:
            _LATD1 = 1;
            break;
          case 4:
            _LATD3 = 1;
            break;
          case 5:
            _LATD2 = 1;
            break;
          default:
            break;
          }
        Delay( Delay_2S_Cnt );  
}

//int nDutyCycle1[10] = {1023, 1000, 998, 995, 991, 986, 960, 860, 760, 248 };
//int nDutyCycle2[10] = {1023, 1000, 998, 995, 991, 986, 960, 880, 800, 512 };
//int nDutyCycle3[10] = {1023, 1000, 998, 995, 991, 986, 960, 512, 256, 0 };
int nDutyCycle3[10] = {1023, 1000, 998, 995, 991, 986, 960, 860, 760, 0 };
int nDutyCycle2[10] = {1023, 1000, 999, 995, 991, 986, 960, 920, 880, 700 };
int nDutyCycle1[10] = {1023, 1000, 998, 995, 991, 986, 960, 800, 700, 512 };

//! main - {one-line description}

int main (void)
{
  int i,n;
  char szWork[32];

  Initialize();

  while (1)
  {
    for (i = 0; i < 10; i++)
      {
        LCDclear ();
        LCDposition (8);
        LCDletter ('0' + i);
        sprintf(szWork," %4d %4d %4d",
                nDutyCycle1[i],nDutyCycle2[i],nDutyCycle3[i]);
        LCDline2();
        LCDputs(szWork);
        OC3RS = nDutyCycle1[i];
        OC4RS = nDutyCycle2[i];
        OC2RS = nDutyCycle3[i];
        Delay (Delay_1S_Cnt/4);
//        Delay (Delay_1S_Cnt);
//        Delay (Delay_1S_Cnt);
//        Delay (Delay_1S_Cnt);
      }

#ifdef MENU
  LCDcommand( 0x0f );   // Turn on cursor
                        // LCD_DISPLAY & LCD_DISP_ON & LCD_CURS_ON
                        // & LCD_BLINK_ON
  while (1)
    {
        LATD |= 0x0e;     // All LEDs off
        LCDclear();
        LCDposition( 21 );
        Delay( Delay_1S_Cnt );
        LCDhome();
        LCDputs(" +Red +Yel +Grn   ");
        LCDposition( 0x40 );
        LCDputs(" -Red -Yel -Grn   ");
        LCDposition( 21 );
        Delay( Delay_1S_Cnt );

        // Do menu actions 0-5
        makeSelection(0);
        makeSelection(1);
        makeSelection(2);
        makeSelection(3);
        makeSelection(4);
        makeSelection(5);

        // Do each row of menu actions from right to left
        makeSelection(2);
        makeSelection(1);
        makeSelection(0);
        makeSelection(5);
        makeSelection(4);
        makeSelection(3);

        // Do each pair - 0,3, 1,4, 2,5
        makeSelection(0);
        makeSelection(3);
        makeSelection(1);
        makeSelection(4);
        makeSelection(2);
        makeSelection(5);

        //Do a dozen random actions
        for ( i=0; i<12; i++ )
          {
            n = rand() / 5461;
            makeSelection( n );
          }

/*
        LCDposition( 0x1 );
        Delay( Delay_500mS_Cnt );
        _LATD1 = 0;
        Delay( Delay_2S_Cnt );
        LCDposition( 0x06 );
        Delay( Delay_500mS_Cnt );
        _LATD3 = 0;
        Delay( Delay_2S_Cnt );
        LCDposition( 0x0b );
        Delay( Delay_500mS_Cnt );
        _LATD2 = 0;
        Delay( Delay_2S_Cnt );
        LCDposition( 0x41 );
        Delay( Delay_500mS_Cnt );
        _LATD1 = 1;
        Delay( Delay_2S_Cnt );
        LCDposition( 0x46 );
        Delay( Delay_500mS_Cnt );
        _LATD3 = 1;
        Delay( Delay_2S_Cnt );
        LCDposition( 0x4b );
        Delay( Delay_500mS_Cnt );
        _LATD2 = 1;
        Delay( Delay_2S_Cnt );
*/
    }
#endif
}
}



