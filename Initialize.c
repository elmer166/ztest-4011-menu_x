#include <xc.h>
#include "../4011-LCDlib-D.X/lcd.h"
void
Initialize (void)
{

  // Turn off LEDs
  LATD |= 0x000e;
  // Make PORTD LED pins outputs
  TRISD &= 0xfff1;

  LCDinit ();

  // Set up timer 2 for PWM
  TMR2 = 0; // Clear timer 2
  PR2 = 1000; // Timer 2 counter to 1000
  T2CON = 0x8010; // Fosc/4, 1:4 prescale, start TMR2

  // Set up PWM on OC2 (RD1)
  OC2RS = 1024; // PWM 2 duty cycle
  OC2R = 0; //
  OC2CON = 0x6; // Set OC2 to PWM mode, timer 2

  // Set up PWM on OC3 (RD2)
  OC3RS = 1024; // PWM 3 duty cycle
  OC3R = 0; //
  OC3CON = 0x6; // Set OC4 to PWM mode, timer 2

  // Set up PWM on OC4 (RD3)
  OC4RS = 1024; // PWM 4 duty cycle
  OC4R = 0; //
  OC4CON = 0x6; // Set OC4 to PWM mode, timer 2

}
